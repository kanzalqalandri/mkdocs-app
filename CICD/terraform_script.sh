#! /bin/bash/


set -e
ls -la
pwd
cd aws-infra/ecr/
# cd "${DIRECTORY}" ||  exit
apt-get update &&  apt-get install -y curl unzip jq python3-pip wget
pip3 install awscli
wget https://releases.hashicorp.com/terraform/1.6.2/terraform_1.6.2_linux_amd64.zip
mv terraform_1.6.2_linux_amd64.zip terraform.zip
unzip terraform.zip && mv terraform /usr/local/bin/
rm terraform.zip
ls -la
pwd
# cd aws-infra/ecr/
ls -la 
pwd
export access_key=$AWS_ACCESS_KEY
export secret_key=$AWS_SECRET_ACCESS_KEY
terraform init
terraform validate
terraform plan
