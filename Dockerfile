# Dockerfile to build the images

# Base image for the app
FROM squidfunk/mkdocs-material

# working dir in the image/container
WORKDIR /docs

# Copy everything into the working dir
COPY . . 

# Expose container port
EXPOSE 8000

# Run the container
CMD ["serve", "--dev-addr=0.0.0.0:8000"]

